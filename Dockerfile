FROM bitnami/kubectl
USER root
COPY kp-linux-0.1.3 /usr/local/bin/kp
COPY ytt-linux-amd64 /usr/local/bin/ytt
COPY kapp-linux-amd64-v0.33.0+vmware.1 /usr/local/bin/kapp
COPY tkg-linux-amd64-v1.2.0+vmware.1 /usr/local/bin/tkg

RUN chmod +x /usr/local/bin/kapp
RUN chmod +x /usr/local/bin/tkg
RUN chmod +x /usr/local/bin/ytt
RUN chmod +x /usr/local/bin/kp

